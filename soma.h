/* 
    Geralmente você vai usar esse #ifndef _ARQUIVO_H_ #define _ARQUIVO_H_ sempre que tiver criando uma biblioteca.
    De um jeito beeem resumido, é um comando pra incluir o arquivo. Posso explicar melhor depois.
*/

#ifndef _SOMA_H_ //pode ser o nome que você quiser, mas é costume usarem o mesmo nome do arquivo
#define _SOMA_H_

int soma(int a, int b); //só a assinatura da função (ou funções)

#endif //complementa os dois comandos lá de cima